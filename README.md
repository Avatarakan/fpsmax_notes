## Single page note-talking application

### Technologies
- React, redux, typescript, scss, webpack, eslint, babel.

### Project scripts
1. `yarn dev` - build developer project in the `dist` directory.
2. `yarn build` - build production project in the `dist` directory.
3. `yarn start` - start hot reloading developer server at the `localhost:4200`.
