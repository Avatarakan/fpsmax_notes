import { ELLIPSIS } from "../constants";


export default function( string: string, subLength: number = 20 )  {
    let subString = string;
    if (string.length > subLength) {
        subString = subString.substring(0, subLength) + ELLIPSIS;
    }
    return subString;
}
