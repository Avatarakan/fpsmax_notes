import { combineReducers, configureStore } from '@reduxjs/toolkit';
import notesSlice from './reducers/notes/notesSlice'

const rootReducer = combineReducers ( {
   notes: notesSlice
} );

export default configureStore ( {
   reducer: rootReducer
} );

export type RootState = ReturnType<typeof rootReducer>
