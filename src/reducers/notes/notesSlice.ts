import { createSlice, nanoid } from '@reduxjs/toolkit';

interface INote {
    id: string,
    title: string,
    content: [ string ],
    isTrashed: boolean,
    date: string
}

const initialState: INote[] = [
    {
        id: '1',
        title: 'Adding the New Post Form',
        content: [ 'Hello first note' ],
        isTrashed: false,
        date: '2020-02-04T08:05:44.498Z'
    },
    {
        id: '2',
        title: 'Second note',
        content: [ 'Second hello world' ],
        isTrashed: false,
        date: '2020-02-04T08:03:44.498Z'
    }
];

const notesSlice = createSlice ( {
    name: 'notes',
    initialState,
    reducers: {
        noteAdded: {
            reducer ( state, action) {
                state.push ( action.payload );
            },
            prepare ( title, content ) {
                return {
                    payload: {
                        id: nanoid(),
                        title,
                        content,
                        date: new Date().toISOString(),
                        isTrashed: false,
                    },
                    meta: {},
                    error: {}
                }
            }
        },
        noteDeleted ( state, action ) {
            const { id } = action.payload;
            const existingNote = state.find( note => note.id === id );
            if ( existingNote ) {
                existingNote.isTrashed = true;
                existingNote.content = [''];
            }
        },
        noteSaved ( state, action ) {
            const { id, title, content } = action.payload;
            const existingNote = state.find( note => note.id === id );
            if (existingNote) {
                existingNote.title = title;
                existingNote.content = content;
            }
        }
    }
});

export const { noteAdded, noteSaved, noteDeleted } = notesSlice.actions;
export default notesSlice.reducer

