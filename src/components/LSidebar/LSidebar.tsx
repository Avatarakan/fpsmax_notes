import React from 'react';
import AddNoteButton from '../AddNoteButton/AddNoteButton';
import NotesList from '../NotesList/NotesList';
import NoteSearch from '../NoteSearch/NoteSearch';
import style from './LSidebar.scss'


function LSidebar () {
    return (
        <div className={ style.sidebar }>
            <AddNoteButton/>
            <NotesList/>
            <NoteSearch/>
        </div>
    )
}

export default LSidebar;
