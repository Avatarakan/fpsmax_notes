import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import style from './NoteForm.scss'
import {
    ADD_NOTE,
    DEFAULT,
    EMPTY_LINE,
    EVENT_KEY,
    INITIAL_NOTE,
    NOTE_NOT_FOUND,
    PAGE_NOT_FOUND,
    VIEW_NOTE
} from '../../constants';
import NoteFormPanel from '../NoteFormPanel/NoteFormPanel';


function NoteForm ( props : any ) {
    const { noteId } = props.pathParameters || ADD_NOTE;

    const selectNotes = ( state : RootState ) => state.notes;
    const notes = useSelector ( selectNotes );

    const [ noteTitle , setNoteTitle ] = useState ( EMPTY_LINE );
    const [ noteContent, setNoteContent ] = useState ( EMPTY_LINE );
    const [ noteContentArray , setNoteContentArray ] = useState ( [ EMPTY_LINE ] );
    const [ interactionType, setInteractionType ] = useState( DEFAULT );

    const [ isEdit, setIsEdit ] = useState( false );

    const notFound = () => {
        setNoteTitle ( PAGE_NOT_FOUND );
        setInteractionType ( NOTE_NOT_FOUND );
    };

    useEffect ( () => {
        if ( noteId === ADD_NOTE ) {
            setNoteTitle ( INITIAL_NOTE.Title.toString () );
            setNoteContent( INITIAL_NOTE.Content.toString () );
            //setNoteContentArray ( [ INITIAL_NOTE.Content.toString () ] );
            setIsEdit (true);
            setInteractionType( ADD_NOTE );
        } else if ( noteId ) {
            setIsEdit (false);
            const viewNote = notes.find ( ( item ) => item.id === noteId && !item.isTrashed );

            if ( viewNote ) {
                setNoteTitle ( viewNote.title );
                // @ts-ignore
                setNoteContentArray ( viewNote.content );
                setNoteContent( viewNote.content.join('\n') );
                setInteractionType( VIEW_NOTE );
            } else {
                notFound()
            }
        } else {
            notFound()
        }
    } , [ noteId, notes ] );

    const keyEnterDisable = ( event : any ) => {
        if ( event.key === EVENT_KEY.Enter ) {
            event.preventDefault ();
        }
    };

    const onTitleChange = ( event : any ) => {
        const newLine = event.target.innerText;
        setNoteTitle ( newLine );
    };

    // display note content
    const displayContentArray = useMemo ( () => {
        const viewContent = noteContentArray
            .map ( ( note , id ) => (
                <div key={ id } >{ note || (<br/>)}</div>
            ));

        return (
            <div className={ style[ 'note-form__content' ] } >
                { viewContent }
            </div>
        )
    } , [ noteContentArray ] );


    const editContent = useMemo ( () => {
        return (
            <textarea
                className={ style[ 'note-form__content' ] }
                onChange={ (event) => setNoteContent(event.target.value)}
                value={noteContent}
            />
        )
    } , [ noteContent , isEdit ] );

    useEffect( () => {
        setNoteContentArray(noteContent.split('\n'));
    }, [noteContent]);

    // initialize parameters for panel
    const panelProps = {
        noteTitle,
        noteContent: noteContentArray,
        type: interactionType,
        id: noteId,
        setNoteTitle,
        setNoteContent: setNoteContent,
        setInteractionType,
        setIsEdit,
    };
    return (
        <div className={ style[ 'note-form' ] }>
            <h2
                className={ style[ 'note-form__title' ] }
                contentEditable={ isEdit }
                onKeyDown={ keyEnterDisable }
                onBlur={ onTitleChange }
            >
                { noteTitle }
            </h2>

            { isEdit ? editContent : displayContentArray }

            <NoteFormPanel props={panelProps}/>
        </div>
    )
}

export default NoteForm;
