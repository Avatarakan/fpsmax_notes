import React from 'react';
import style from './RContent.scss'
import NoteForm from '../NoteForm/NoteForm';

function RContent ( { match } : any ) {
    return (
        <div className={ style.content }>
            <NoteForm pathParameters={ match.params } />
        </div>
    )
}

export default RContent;
