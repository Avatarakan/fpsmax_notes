import React from 'react';
import style from './NoteSearch.scss'

function NoteSearch () {

    return(
        <div className={ style['note-search'] }>
            <input placeholder={ 'Search everywhere' } className={ style['note-search__input'] }/>
        </div>
    )
}

export default NoteSearch;
