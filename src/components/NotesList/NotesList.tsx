import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import cx from 'classnames'
import substringPointer from '../../helpers/substringPointer';
import { RootState } from '../../store';
import style from './NotesList.scss';

function NotesList () {
    const selectNotes = ( state: RootState ) => state.notes;
    const notes = useSelector ( selectNotes );
    const notesCount = notes.slice().filter((item) => !item.isTrashed).length;
    const notesTrashedCount = notes.slice().filter((item) => item.isTrashed).length;
    const orderedNotes = notes.slice().sort((a, b) => b.date.localeCompare(a.date));

    const formatNotes = orderedNotes
        .filter( ( item ) =>  !item.isTrashed )
        .map ( ( item ) => {
            return (
                <Link to={ `/notes/${ item.id }` }>
                    <div key={ item.id } className={ style[ 'notes-list__item' ] }>
                        <h4>{ substringPointer ( item.title , 30 ) }</h4>
                        <span> <img src={ '/icons/ellipsis.svg' } alt={ 'More details' }/></span>
                    </div>
                </Link>
            )
        } );

    const notesTrashed = useMemo( () => {

        const formatTrashedNotes = orderedNotes
            .filter( ( item ) =>  item.isTrashed)
            .map( ( item ) => {
                return (
                    <div key={ item.id } className={ style[ 'notes-list__item' ] }>
                        <h4>{ substringPointer ( item.title , 30 ) }</h4>
                    </div>
                )
            } );

        if(notesTrashedCount) {
            return (
                <>
                    <div className={ cx( style[ 'notes-list__title' ], style['trash']) }>
                        <h3>{ notesTrashedCount } trashed notes:</h3>
                        <span/>
                    </div>
                    {formatTrashedNotes}
                </>
            )
        }
    }, [notesTrashedCount, orderedNotes]);

    return (
        <div className={ style[ 'notes-list' ] }>
            <div className={ style[ 'notes-list__title' ] }>
                <h3>{ notesCount } notes:</h3>
                <span/>
            </div>
            { formatNotes }
            { notesTrashed }

        </div>
    );
}

export default NotesList;
