import React from 'react';
import { Link } from 'react-router-dom';
import style from './AddNoteButton.scss';

function AddNoteButton () {

    return (
        <Link to={ `/notes/add-note` }>
            <div className={ style['notes-list__item'] }>
                <h4>Add new note</h4>
                <span>
                    <img src={ '/icons/plus.svg' } alt={'Add new note'}/>
                </span>
            </div>
        </Link>
    )
}

export default AddNoteButton;
