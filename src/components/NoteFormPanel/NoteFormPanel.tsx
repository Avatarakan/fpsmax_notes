import React, { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { noteAdded, noteDeleted, noteSaved } from '../../reducers/notes/notesSlice'
import style from './NoteFormPanel.scss'
import { ADD_NOTE, BUTTON_TITLE, EDIT_NOTE, INITIAL_NOTE, VIEW_NOTE } from '../../constants';


interface INoteFormPanel {
    props: {
        noteTitle: string,
        noteContent: string[],
        type: string,
        id?: string,
        setNoteTitle: React.Dispatch<React.SetStateAction<string>>,
        setNoteContent: React.Dispatch<React.SetStateAction<string>>,
        setInteractionType: React.Dispatch<React.SetStateAction<string>>,
        setIsEdit: React.Dispatch<React.SetStateAction<boolean>>,
    }
}

interface IButton {
    title: BUTTON_TITLE,
    type: string[],
    action: () => void
}

function NoteFormPanel ( {props}: INoteFormPanel ) {
    const dispatch = useDispatch ();

    const noteClear = () => {
        props.setNoteTitle ( INITIAL_NOTE.Title.toString () );
        props.setNoteContent ( INITIAL_NOTE.Content );
    };

    const noteAdd = () => {
        dispatch ( noteAdded ( props.noteTitle, props.noteContent ) );
        props.setIsEdit(false);
        noteClear();
    };

    const noteSave = () => {
        if( props.id ) {
            dispatch( noteSaved ( {
                id: props.id,
                title: props.noteTitle,
                content: props.noteContent
            } ) );

            props.setIsEdit(false);
            props.setInteractionType( VIEW_NOTE )
        }
    };

    const noteDelete = () => {
        if ( props.id) {
            dispatch( noteDeleted ({ id: props.id }) );
            props.setNoteContent('');
        }
    };

    const noteEdit = () => {
        props.setIsEdit(true);
        props.setInteractionType( EDIT_NOTE )
    };


    const panelButtons:IButton[] = [
        {
            title: BUTTON_TITLE.Edit,
            type: [VIEW_NOTE],
            action: noteEdit
        },
        {
            title: BUTTON_TITLE.Add,
            type: [ADD_NOTE],
            action: noteAdd
        },
        {
            title: BUTTON_TITLE.Save,
            type: [EDIT_NOTE],
            action: noteSave
        },
        {
            title: BUTTON_TITLE.Clear,
            type: [ADD_NOTE, EDIT_NOTE],
            action: noteClear
        },
        {
            title: BUTTON_TITLE.Delete,
            type: [EDIT_NOTE, VIEW_NOTE],
            action: noteDelete
        },
    ];

    const displayPanelButton = useMemo(() => {
        const buttonElement =  panelButtons
            .filter((button) => button.type.indexOf(props.type) !== -1 )
            .map(( button ) => (
                <span className={style['note-form-panel__button']} onClick={button?.action}>
                    {button.title.toUpperCase()}
                </span>
            ));

        return (
            <>
                { buttonElement }
            </>
        )
    }, [panelButtons]);

    return (
        <div className={style['note-form-panel']}>
            { displayPanelButton }
        </div>
    )
}

export default NoteFormPanel;
