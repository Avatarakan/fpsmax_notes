export const ELLIPSIS = '...';

export enum INITIAL_NOTE {
    Title = 'Untitled',
    Content = 'Input note text there',
    Paragraph = 'New paragraph'
}

export enum EVENT_KEY {
    Enter = 'Enter',
    Backspace = 'Backspace',
}

export enum BUTTON_TITLE {
    Edit = 'edit',
    Add = 'add',
    Save = 'save',
    Clear = 'clear',
    Delete = 'delete'
}

export const VIEW_NOTE = 'view-note';
export const ADD_NOTE = 'add-note';
export const EDIT_NOTE = 'edit-note';
export const NOTE_NOT_FOUND = 'not-found';
export const DEFAULT = 'default';
export const PAGE_NOT_FOUND = 'Page not found';
export const EMPTY_LINE = '';
