import React from 'react';
import {
    BrowserRouter as Router ,
    Switch ,
    Route ,
    Redirect
} from 'react-router-dom'

import styleApp from './styles/App.scss'
import LSidebar from './components/LSidebar/LSidebar';
import RContent from './components/RContent/RContent';

function App () {
    return (
        <Router>
            <div className={ styleApp.wrapper }>
                <LSidebar/>
                <Switch>
                    <Route
                        exact
                        path={ [ '/notes/:noteId' , '/notes/add-note' ] }
                        component={ RContent }
                    />
                    <Redirect to={ '/notes/add-note' }/>
                </Switch>
            </div>
        </Router>
    )
}

export default App;
