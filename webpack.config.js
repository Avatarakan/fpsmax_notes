const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
let webpack = require('webpack');

const isDevelopment = process.env.NODE_ENV === 'development';
const isProduction = !isDevelopment;

const plugins = [
    new CleanWebpackPlugin(),
    new HTMLWebpackPlugin({
        template: './public/index.html',
        minify: {
            collapseWhitespace: isProduction
        }
    }),
    new CopyWebpackPlugin({
        patterns: [
            {
                from: 'public',
                to: path.resolve(__dirname, 'dist'),
            }
        ]
    }),
    new MiniCssExtractPlugin({
        filename: '[name].[contenthash].scss',
    }),
    new ESLintPlugin(),
];

const optimization = {
    splitChunks: {
        chunks: 'all'
    },
};

if (isDevelopment) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
} else if (isProduction) {
    optimization.minimize = true;
    optimization.minimizer = [
        new CssMinimizerPlugin(),
        new TerserPlugin(),
    ]
}

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        index: ['@babel/polyfill', '/index.tsx']
    },
    mode: 'development',
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },
    output: {
        filename: '[name].[contenthash].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    plugins,
    optimization,
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-modules-typescript-loader',
                        options: {
                            mode: process.env.CI ? 'verify' : 'emit'
                        }
                    },
                    {
                        loader: 'css-loader',
                        options: {modules: true},
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(png|svg|jpg)$/,
                use: ['file-loader']
            },
            {
                test: /\.tsx?$/,
                use: ['ts-loader']
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname,'dist'),
        compress: true,
        port: 4200,
        watchContentBase: true,
        progress: true,
        hot: isDevelopment,
        historyApiFallback: true,
    },
    devtool: isDevelopment && 'source-map',
};
