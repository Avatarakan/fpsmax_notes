## 04.02.2021
### Create textarea note editor 
* Fix

1. Created textarea component for editing note `src/components/NoteForm/NoteForm.tsx`, `src/components/NoteForm/NoteForm.scss`.
2. Edited note form panel `src/components/NoteFormPanel/NoteFormPanel.tsx`.

### Create notes panel
* Feature

1. Created functional for saving, deleting, clearing and adding notes in store with `NoteFormPanel` component `src/components/NoteForm/NoteForm.tsx`, `src/components/NoteForm/NoteForm.scss` and reducers from `src/reducers/notes/notesSlice.ts`.
2. Added literal constants and upgrade it in files project `src/constants/index.ts`.


## 03.02.2021
### Create navigation and content displaying components
* Feature

1. Added main left sidebar block `src/components/LSidebar/LSidebar.tsx` with style `src/components/LSidebar/LSidebar.scss`
and right block for displaying app content `src/components/RContent/RContent.tsx` `src/components/RContent/RContent.scss`.
2. Added component for displaying at the left sidebar the navigation for all notes contained in the app `src/components/AddNoteButton/AddNoteButton.tsx`,`src/components/AddNoteButton/AddNoteButton.scss` 
and addition new notes `src/components/AddNoteButton/AddNoteButton.tsx`, `src/components/AddNoteButton/AddNoteButton.scss`.
3. Added component for displaying and editing notes `src/components/NoteForm/NoteForm.tsx`, `src/components/NoteForm/NoteForm.scs`.
4. Moved the literal constants to a separate file `src/constants/index.ts`.
5. Added static files and HTML template `src/public/fonts`, `src/public/icons`, `src/public/index.html`.
6. Added redux slice for working with the storage of notes `src/reducers/notes/notesSlice.ts`.


## 01.02.2021
### Initial project
* Feature

1. Configured environment `Webpack^5.19.0`, `TypeScript^4.1.3`, `Eslint^7.19.0`, `@babel/core^7.12.10`, `react^17.0.1`, `redux^4.0.5`, `sass^1.32.5`.
2. Created the primary directory structure of the project.
3. Added `src/index.tsx`,`src/App.tsx` main components with routing, initialized redux store `store.ts`.
4. Added common styles `src/styles/App.scss`, `src/styles/fonts.scss`, `src/styles/constants.scss`.


